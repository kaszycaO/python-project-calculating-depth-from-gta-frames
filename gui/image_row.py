import kivy
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.behaviors import ButtonBehavior


class ImageRow(BoxLayout):
    """ ROW OF IMAGES IN SCROLL PANEL """

    def __init__(self):
        super(ImageRow, self).__init__()
        self.orientation = "horizontal"
        self.size_hint_y = None
        self.height = 250
