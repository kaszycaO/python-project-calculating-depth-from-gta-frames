from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label


class ImageLabels(BoxLayout):
    def __init__(self):
        super(ImageLabels, self).__init__()
        self.orientation = "horizontal"
        self.size_hint = 1, 0.2
        self.add_components()

    def add_components(self):
        self.add_widget(Label(text="Game Image"))
        self.add_widget(Label(text="Depth Image"))
