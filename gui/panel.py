from kivy.uix.boxlayout import BoxLayout
from top_bar import TopBar
from image_panel import ImagePanel
from text_panel import TextPanel
from image_labels import ImageLabels
from clear_panel_btn import ClearPanelButtonBar


class Panel(BoxLayout):
    """MAIN PANEL"""

    def __init__(self):
        super(Panel, self).__init__()
        self.orientation = "vertical"
        self.set_components()
        self.add_components()

    def set_components(self):
        self.image_labels = ImageLabels()
        self.text_panel = TextPanel()
        self.image_panel = ImagePanel(self.text_panel)
        self.clear_panel_button_bar = ClearPanelButtonBar(self.image_panel)
        self.top_bar = TopBar(self.text_panel, self.image_panel)

    def add_components(self):
        self.add_widget(self.top_bar)
        self.add_widget(self.image_labels)
        self.add_widget(self.image_panel)
        self.add_widget(self.clear_panel_button_bar)
        self.add_widget(self.text_panel)
