import kivy

from kivy.core.window import Window
from kivy.uix.popup import Popup

from popup_panel import PicturePanel


class DragDropPopup(Popup):
    """ External popup window used for uploading picures """

    def __init__(self, text_panel, predict_btn, image_panel):
        super(DragDropPopup, self).__init__()

        self.title = "Drag file here"
        self.content = PicturePanel(self, text_panel, predict_btn, image_panel)
        self.size_hint = (None, None)
        self.size = (500, 500)
        self.auto_dismiss = False
        self.bind(on_dismiss=self.content.send_info)
        Window.bind(on_dropfile=self._on_file_drop)
        self.open()

    def _on_file_drop(self, window, source):
        self.content.set_backround(source.decode("utf-8"))
