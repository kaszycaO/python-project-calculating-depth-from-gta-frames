from kivy.uix.spinner import Spinner
from kivy.uix.button import Button
from kivy.uix.label import Label
import os


class MySpinner(Spinner):
    """ Run 360 """

    def __init__(self, text_panel):
        super(MySpinner, self).__init__()
        self.text_panel = text_panel
        self.background_color = 0, 1, 0, 0.5
        self.text = "Run 360"
        self.bind(text=self.show_selected_value)

        # you can add more here
        self.models = (
            (
                "Scenario 1",
                [
                    "./sample_images/360_5/1.png",
                    "./sample_images/360_5/2.png",
                    "./sample_images/360_5/3.png",
                    "./sample_images/360_5/4.png",
                ],
            ),
            (
                "Scenario 2",
                [
                    "./sample_images/360_0/1.png",
                    "./sample_images/360_0/2.png",
                    "./sample_images/360_0/3.png",
                    "./sample_images/360_0/4.png",
                ],
            ),
            (
                "Scenario 3",
                [
                    "./sample_images/360_1/1.png",
                    "./sample_images/360_1/2.png",
                    "./sample_images/360_1/3.png",
                    "./sample_images/360_1/4.png",
                ],
            ),
        )
        self.values = tuple([el[0] for el in self.models])

    def show_selected_value(self, *args):
        if self.text == "Load 360":
            pass
        else:
            # self.text_panel.text += "Loading, please wait!" + '\n'
            self.get_option(self.text)

    def get_option(self, model_name):
        for el in self.models:
            if model_name == el[0]:
                os.system(
                    "python3 run_showcase.py {} {} {} {}".format(
                        el[1][0], el[1][1], el[1][2], el[1][3]
                    )
                )
                break
