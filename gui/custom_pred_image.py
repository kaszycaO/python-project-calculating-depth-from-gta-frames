from kivy.uix.image import Image
from kivy.uix.behaviors import ButtonBehavior
import ntpath


class CustomPredImage(Image, ButtonBehavior):
    """ IMAGE AFTER PREDICTION WITH SAVE ON-CLICK OPTION"""

    def __init__(self, texture):
        super(CustomPredImage, self).__init__()
        self.texture = texture
        self.size = (224, 224)
        self.panel = None

    def on_touch_down(self, touch):
        # saving photo
        if self.collide_point(*touch.pos) and self.panel:
            index = self.panel.post_prediction.index(self)
            image_file_path, image_file_name = ntpath.split(self.panel.file_path[index])
            pred_file_name = "pred_" + image_file_name
            self.panel.text_panel.text += "Row [{0}]: Saving prediction: {1} \n \
                 for image:  {2} \n \
                 in current location:  {3} \n".format(
                index + 1, pred_file_name, image_file_name, image_file_path
            )
            # saving code
