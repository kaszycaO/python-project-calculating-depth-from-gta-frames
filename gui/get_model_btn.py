from kivy.uix.button import Button
from model_list import ModelList
import torch


class GetModelButton(Button):
    """MODEL LOADING CLASS"""

    def __init__(self, model_list, text_panel, predict_btn):
        super().__init__()
        self.text = "Load model"
        self.model_list = model_list
        self.text_panel = text_panel
        self.predict_btn = predict_btn

    def on_release(self):
        model_name = self.model_list.text
        if model_name == "Models":
            self.text_panel.text += "No model chosen \n"
        else:
            self.text_panel.text += "Chosen model: " + str(model_name) + "\n"
            self.get_model(model_name)

    def get_model(self, model_name):
        for el in self.model_list.models:
            if model_name == el[0]:
                model = torch.load(el[1], map_location=torch.device("cpu"))
                self.predict_btn.change_model(model, model_name)
                break
        else:
            self.text_panel.text += "Model not found\n"
