from kivy.uix.button import Button

from drag import DragDropPopup


class GetImageButton(Button):
    def __init__(self, text_panel, predict_btn, image_panel):
        super().__init__()
        self.text = "Upload image"
        self.text_panel = text_panel
        self.predict_btn = predict_btn
        self.image_panel = image_panel

    def on_release(self):
        DragDropPopup(self.text_panel, self.predict_btn, self.image_panel)
