from kivy.uix.spinner import Spinner
from kivy.uix.button import Button


class ModelList(Spinner):
    """LIST OF MODELS AND PATHS TO MODELS,
    HERE YOU CAN CHANGE IT"""

    def __init__(self):
        super(ModelList, self).__init__()
        self.text = "Models"
        #
        # Name, Path
        #
        self.models = (
            ('DenseNet', "./models/densenet121_adamw"),
            ('ResNet', "./models/resnet34_adamw"),
            ('EfficientNet', "./models/efficient_b4")
        )
        self.values = tuple([el[0] for el in self.models])
