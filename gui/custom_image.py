from kivy.uix.image import Image
from kivy.uix.behaviors import ButtonBehavior
from delete_confirmation_popup import DeleteConfirmationPopup


class CustomImage(Image, ButtonBehavior):
    """ IMAGE WITH ON-CLICK DELETE OPTION """

    def __init__(self, source):
        super(CustomImage, self).__init__()
        self.source = source

    def on_touch_down(self, touch):
        # deleting photo and predction if exists
        panel = self.parent.parent.parent
        if self.collide_point(*touch.pos):
            DeleteConfirmationPopup(image=self, image_panel=panel, clear_all=False)

    def confirm_deleting(self):
        panel = self.parent.parent.parent
        if panel.post_prediction:
            panel.post_prediction.pop(panel.file_path.index(self.source))
        panel.file_path.remove(self.source)
        panel.refresh(panel.file_path)
        panel.refresh_pred(panel.post_prediction)
