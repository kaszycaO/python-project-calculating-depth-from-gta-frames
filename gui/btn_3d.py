from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.core.window import Window
from kivy.uix.popup import Popup

import os


class Run3DButton(Button):
    def __init__(self):
        super().__init__()
        self.text = "3D"
        self.background_color = 1, 0, 0, 1

    def on_release(self):
        MyPopup()


class MyPopup(Popup):
    """ External popup window used for uploading picures """

    def __init__(self):
        super(MyPopup, self).__init__()

        self.title = "Drag file here"
        self.content = PicturePanel(self)
        self.size_hint = (None, None)
        self.size = (500, 500)
        self.auto_dismiss = False
        Window.bind(on_dropfile=self._on_file_drop)
        self.open()

    def _on_file_drop(self, window, source):
        self.content.set_backround(source.decode("utf-8"))


class OptionButton(Button):
    """ Panel with options: CONFIRM and CANCEL """

    def __init__(self, **kwargs):
        super(OptionButton, self).__init__(**kwargs)
        self.size_hint_y = None
        self.height = 50


class PhotosPanel(GridLayout):
    """ Panel with dragged picture"""

    def __init__(self, **kwargs):
        super(PhotosPanel, self).__init__(**kwargs)
        self.cols = 3
        self.size_hint_y = None
        self.height = 300

    def add_image(self, file_path):
        self.clear_widgets(self.children)
        self.add_widget(MyImage(file_path))


class MyImage(Image):
    """ Custom image with event"""

    def __init__(self, source):
        super(MyImage, self).__init__()
        self.source = source


class PicturePanel(GridLayout):
    """ Main panel with popup components """

    def __init__(self, popup):
        super(PicturePanel, self).__init__()

        self.file_path = ""
        self.popup = popup

        self.cols = 1
        self.padding = 10

        self.background_color = 0.1, 0.1, 0.1, 1
        self.foreground_color = 1, 1, 1, 1

        self.set_components()
        self.add_components()

    def set_components(self):
        self.panel = GridLayout(cols=3, padding=10)
        self.confirm_button = OptionButton(text="CONFIRM")
        self.cancel_button = OptionButton(text="CANCEL")
        self.confirm_button.bind(on_press=self.save_photo)
        self.cancel_button.bind(on_press=self.cancel)

        self.panel.add_widget(self.cancel_button)
        self.panel.add_widget(self.confirm_button)

        self.bg_panel = PhotosPanel()

    def add_components(self):
        self.add_widget(self.bg_panel)
        self.add_widget(self.panel)

    def set_backround(self, file_path):
        """ Show photo in PhotosPanel """
        self.file_path = file_path
        self.bg_panel.add_image(file_path)

    def cancel(self, _):
        """ Action for CANCEL button """
        self.popup.dismiss()

    def save_photo(self, _):
        """ Action for CONFIRM button """
        if self.file_path != "":
            self.popup.dismiss()
            # po chmod + x mozna zmienic na ./run_showcase.py
            os.system("python3 run_showcase.py {}".format(self.file_path))
