import kivy
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from model_list import ModelList
from get_model_btn import GetModelButton
from predict_btn import PredictButton
from get_image_btn import GetImageButton
from save_pred_btn import SavePredBtn
from btn_3d import Run3DButton
from btn_360 import MySpinner


class TopBar(BoxLayout):
    """UPPER OPTION BAR"""

    def __init__(self, text_panel, image_panel):
        super(TopBar, self).__init__()
        self.orientation = "horizontal"
        self.text_panel = text_panel
        self.size_hint = 1, 0.05
        self._set_components(image_panel)
        self._add_components()

    def _set_components(self, image_panel):
        self.model_list = ModelList()
        self.predict_btn = PredictButton(self.text_panel, image_panel)
        self.get_image_btn = GetImageButton(
            self.text_panel, self.predict_btn, image_panel
        )
        self.get_model_btn = GetModelButton(
            self.model_list, self.text_panel, self.predict_btn
        )
        self.save_pred_btn = SavePredBtn(self.text_panel, image_panel)
        self.animation_btn = Run3DButton()
        self.spinner360 = MySpinner(self.text_panel)

    def _add_components(self):
        # self.add_widget(Label())
        self.add_widget(self.model_list)
        self.add_widget(self.get_model_btn)
        self.add_widget(self.get_image_btn)
        self.add_widget(self.predict_btn)
        self.add_widget(self.save_pred_btn)
        self.add_widget(self.animation_btn)
        self.add_widget(self.spinner360)
