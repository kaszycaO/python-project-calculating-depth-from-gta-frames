import kivy
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.image import Image
from image_row import ImageRow
from custom_image import CustomImage


class ImagePanel(ScrollView):
    """IMAGE VIEWER"""

    def __init__(self, text_panel):
        super(ImagePanel, self).__init__()
        self.do_scroll_y = True
        self.elements = 0
        self.rows = []
        self._fill_image_panel()
        self.file_path = []
        self.text_panel = text_panel
        self.post_prediction = []

    def _fill_vertical_panel(self):
        for _ in range(self.elements):
            temp = ImageRow()
            self.rows.append(temp)
            self.vertical_panel.add_widget(temp)

    def _fill_image_panel(self, using_file_paths=True):
        if using_file_paths:
            self.vertical_panel = GridLayout(cols=1, spacing=10, size_hint_y=None)
            self.vertical_panel.bind(
                minimum_height=self.vertical_panel.setter("height")
            )
            self._fill_vertical_panel()
            for i in range(self.elements):
                self.rows[i].add_widget(CustomImage(self.file_path[i]))
            self.add_widget(self.vertical_panel)
        else:
            for i in range(len(self.post_prediction)):
                self.rows[i].add_widget(self.post_prediction[i])

    #
    # pre and post prediction are lists of kivy.uix.image
    #
    def refresh_pred(self, post_prediction):
        self.post_prediction = post_prediction
        self._fill_image_panel(False)

    def refresh(self, file_path):
        self.clear_widgets(self.children)
        self.elements = len(file_path)
        self.rows = []
        self.file_path = file_path
        self._fill_image_panel()
        if self.post_prediction:
            for p in self.post_prediction:
                p.parent = None

    def clear_panel(self):
        self.elements = 0
        self.file_path.clear()
        self.rows.clear()
        self.post_prediction.clear()
        self.refresh([])

    def get_predictions(self):
        return self.post_prediction
