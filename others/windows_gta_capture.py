from PIL import Image
from PIL import ImageGrab
import win32gui
import keyboard
import time
import numpy as np
import os
from pathlib import Path
import argparse

parser = argparse.ArgumentParser(description="Processing input params")
parser.add_argument(
    "--limit", type=int, default=80000, help="Hard limit of images captured"
)
parser.add_argument(
    "--game_save_dir",
    type=str,
    default="D:\\dataset\\360_foot_0\\game",
    help="Save directory",
)
parser.add_argument(
    "--depth_save_dir",
    type=str,
    default="D:\\dataset\\360_foot_0\\depth",
    help="Save directory",
)
parser.add_argument("--window_name", type=str, default="Grand Theft Auto V")
parser.add_argument("--wait_time", type=float, default=0.2)
args = parser.parse_args()

DEPTH_SAVE_PATH = args.depth_save_dir
GAME_SAVE_PATH = args.game_save_dir
WINDOW_NAME = args.window_name
CAPTURE_LIMIT = args.limit
WAIT_TIME = args.wait_time
TARGET_SIZE = 224
SAVE_BATCH = 500

print("Shift + F8: Start capture")
print("Shift + F9: Stop capture")
print("Shift + F10: Abort")
print("\nWaiting for input...")


def capture():
    win32gui.SetForegroundWindow(hwnd)
    dimensions = win32gui.GetWindowRect(hwnd)
    image = ImageGrab.grab(dimensions)

    width, height = image.width, image.height

    depth_image0 = image.crop((0, 0, width // 4, height // 2))
    depth_image0 = depth_image0.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)
    game_image0 = image.crop((0, height // 2 + 1, width // 4, height))
    game_image0 = game_image0.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)

    depth_image1 = image.crop((width // 4 + 1, 0, 2 * width // 4, height // 2))
    depth_image1 = depth_image1.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)
    game_image1 = image.crop((width // 4 + 1, height // 2 + 1, 2 * width // 4, height))
    game_image1 = game_image1.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)

    depth_image2 = image.crop((2 * width // 4 + 1, 0, 3 * width // 4, height // 2))
    depth_image2 = depth_image2.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)
    game_image2 = image.crop(
        (2 * width // 4 + 1, height // 2 + 1, 3 * width // 4, height)
    )
    game_image2 = game_image2.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)

    depth_image3 = image.crop((3 * width // 4 + 1, 0, width, height // 2))
    depth_image3 = depth_image3.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)
    game_image3 = image.crop((3 * width // 4 + 1, height // 2 + 1, width, height))
    game_image3 = game_image3.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)

    return (
        depth_image0,
        depth_image1,
        depth_image2,
        depth_image3,
        game_image0,
        game_image1,
        game_image2,
        game_image3,
    )


def start_capture():
    print("starting capture...")
    global hwnd
    global capturing
    capturing = True
    hwnd = win32gui.FindWindow(None, WINDOW_NAME)


def stop_capture():
    print("\nstopping capturing...")
    global capturing
    capturing = False


def end_session():
    exit(0)


keyboard.add_hotkey("shift + f8", start_capture)
keyboard.add_hotkey("shift + f9", stop_capture)
keyboard.add_hotkey("shift + f10", end_session)

session_timestamp = str(int(time.time() * 100))
capturing = False
captured_count = 0
last_image0 = None
last_image1 = None
last_image2 = None
last_image3 = None

while captured_count < CAPTURE_LIMIT:
    if not capturing:
        time.sleep(1.0)
        continue

    print("\r captured:", captured_count, end="")
    timestamp = int(time.time() * 100)
    depth_image0, depth_image1, depth_image2, depth_image3, game_image0, game_image1, game_image2, game_image3 = (
        capture()
    )

    if last_image0 is not None:
        if (np.asarray(depth_image0) - np.asarray(last_image0)).sum() > 2000000:

            depth_image_path = os.path.join(
                DEPTH_SAVE_PATH,
                session_timestamp + "_" + str(captured_count // SAVE_BATCH),
            )
            game_image_path = os.path.join(
                GAME_SAVE_PATH,
                session_timestamp + "_" + str(captured_count // SAVE_BATCH),
            )
            Path(depth_image_path).mkdir(parents=True, exist_ok=True)
            Path(game_image_path).mkdir(parents=True, exist_ok=True)
            depth_image0.save(
                os.path.join(
                    depth_image_path,
                    str(timestamp) + "_" + str(captured_count) + "_d.png",
                )
            )
            game_image0.save(
                os.path.join(
                    game_image_path,
                    str(timestamp) + "_" + str(captured_count) + "_g.png",
                )
            )
            captured_count += 1
            # time.sleep(WAIT_TIME)

    if last_image1 is not None:
        if (np.asarray(depth_image1) - np.asarray(last_image1)).sum() > 2000000:

            depth_image_path = os.path.join(
                DEPTH_SAVE_PATH,
                session_timestamp + "_" + str(captured_count // SAVE_BATCH),
            )
            game_image_path = os.path.join(
                GAME_SAVE_PATH,
                session_timestamp + "_" + str(captured_count // SAVE_BATCH),
            )
            Path(depth_image_path).mkdir(parents=True, exist_ok=True)
            Path(game_image_path).mkdir(parents=True, exist_ok=True)
            depth_image1.save(
                os.path.join(
                    depth_image_path,
                    str(timestamp) + "_" + str(captured_count) + "_d.png",
                )
            )
            game_image1.save(
                os.path.join(
                    game_image_path,
                    str(timestamp) + "_" + str(captured_count) + "_g.png",
                )
            )
            captured_count += 1
            # time.sleep(WAIT_TIME)

    if last_image2 is not None:
        if (np.asarray(depth_image2) - np.asarray(last_image2)).sum() > 2000000:

            depth_image_path = os.path.join(
                DEPTH_SAVE_PATH,
                session_timestamp + "_" + str(captured_count // SAVE_BATCH),
            )
            game_image_path = os.path.join(
                GAME_SAVE_PATH,
                session_timestamp + "_" + str(captured_count // SAVE_BATCH),
            )
            Path(depth_image_path).mkdir(parents=True, exist_ok=True)
            Path(game_image_path).mkdir(parents=True, exist_ok=True)
            depth_image2.save(
                os.path.join(
                    depth_image_path,
                    str(timestamp) + "_" + str(captured_count) + "_d.png",
                )
            )
            game_image2.save(
                os.path.join(
                    game_image_path,
                    str(timestamp) + "_" + str(captured_count) + "_g.png",
                )
            )
            captured_count += 1
            # time.sleep(WAIT_TIME)

    if last_image3 is not None:
        if (np.asarray(depth_image3) - np.asarray(last_image3)).sum() > 2000000:

            depth_image_path = os.path.join(
                DEPTH_SAVE_PATH,
                session_timestamp + "_" + str(captured_count // SAVE_BATCH),
            )
            game_image_path = os.path.join(
                GAME_SAVE_PATH,
                session_timestamp + "_" + str(captured_count // SAVE_BATCH),
            )
            Path(depth_image_path).mkdir(parents=True, exist_ok=True)
            Path(game_image_path).mkdir(parents=True, exist_ok=True)
            depth_image3.save(
                os.path.join(
                    depth_image_path,
                    str(timestamp) + "_" + str(captured_count) + "_d.png",
                )
            )
            game_image3.save(
                os.path.join(
                    game_image_path,
                    str(timestamp) + "_" + str(captured_count) + "_g.png",
                )
            )
            captured_count += 1

    last_image0 = depth_image0
    last_image1 = depth_image1
    last_image2 = depth_image2
    last_image3 = depth_image3
    time.sleep(WAIT_TIME)

print("\ndone capturing...")

keyboard.wait("esc")
