#pragma once

#include "math.h"
#include "cmath"
#include "../../inc/types.h"

#include "glm/glm.hpp"
#include "glm/gtc/quaternion.hpp"

#define DEG2RAD 0.017453292f

struct v3
{
	float x;
	float y;
	float z;

	v3(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	v3(Vector3 v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
	}
};


v3 rotate_around_x(v3 v, float angle)
{
	angle = angle * DEG2RAD;
	return v3
	{
		v.x,
		v.y * cosf(angle) - v.z * sinf(angle),
		v.y * sinf(angle) + v.z * cosf(angle)
	};
}

v3 rotate_around_y(v3 v, float angle)
{
	angle = angle * DEG2RAD;
	return v3
	{
		v.x * cosf(angle) + v.z * sinf(angle),
		v.y,
		- v.x * sinf(angle) + v.z * cosf(angle)
	};
}

v3 rotate_around_z(v3 v, float angle)
{
	angle = angle * DEG2RAD;
	return v3
	{
		v.x * cosf(angle) - v.y * sinf(angle),
		v.x * sinf(angle) + v.y * cosf(angle),
		v.z,
	};
}

v3 rotate_around(v3 v, v3 other)
{
	v = rotate_around_y(v, other.y);
	v = rotate_around_x(v, other.x);
	v = rotate_around_z(v, other.z);
	return v;
}

glm::vec3 glm_rotate_around(v3 v, v3 other)
{
	v = rotate_around_y(v, other.y);
	v = rotate_around_x(v, other.x);
	v = rotate_around_z(v, other.z);
	return glm::vec3(v.x, v.y, v.z);
}

glm::quat from_euler_angles(float x, float y, float z)
{
	return glm::angleAxis(x * DEG2RAD, glm::vec3(1, 0, 0)) * glm::angleAxis(z * DEG2RAD, glm::vec3(0, 0, 1)) * glm::angleAxis(y * DEG2RAD, glm::vec3(0, 1, 0));
}

struct Quaternion
{
	double w, x, y, z;
};

Quaternion ToQuaternion(double yaw, double pitch, double roll) // yaw (Z), pitch (Y), roll (X)
{
	// Abbreviations for the various angular functions
	double cy = cos(yaw * 0.5);
	double sy = sin(yaw * 0.5);
	double cp = cos(pitch * 0.5);
	double sp = sin(pitch * 0.5);
	double cr = cos(roll * 0.5);
	double sr = sin(roll * 0.5);

	Quaternion q;
	q.w = cr * cp * cy + sr * sp * sy;
	q.x = sr * cp * cy - cr * sp * sy;
	q.y = cr * sp * cy + sr * cp * sy;
	q.z = cr * cp * sy - sr * sp * cy;

	return q;
}

struct EulerAngles {
	double roll, pitch, yaw;
};

#define M_PI 3.14159265359f

EulerAngles ToEulerAngles(Quaternion q) {
	EulerAngles angles;

	// roll (x-axis rotation)
	double sinr_cosp = 2 * (q.w * q.x + q.y * q.z);
	double cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y);
	angles.roll = std::atan2(sinr_cosp, cosr_cosp);

	// pitch (y-axis rotation)
	double sinp = 2 * (q.w * q.y - q.z * q.x);
	if (std::abs(sinp) >= 1)
		angles.pitch = std::copysign(M_PI / 2, sinp); // use 90 degrees if out of range
	else
		angles.pitch = std::asin(sinp);

	// yaw (z-axis rotation)
	double siny_cosp = 2 * (q.w * q.z + q.x * q.y);
	double cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z);
	angles.yaw = std::atan2(siny_cosp, cosy_cosp);

	return angles;
}