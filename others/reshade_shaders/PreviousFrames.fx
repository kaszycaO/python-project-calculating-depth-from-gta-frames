#include "ReShade.fxh"


uniform int iUIFrameOffset <
ui_label = "FrameOffset";
> = 0;

texture	texCompleteFrame
{
	Width = BUFFER_WIDTH;
	Height = BUFFER_HEIGHT;
	Format = RGBA8;
};
sampler	samplerCompleteFrame
{
	Texture = texCompleteFrame;
};

texture	texPreviousCompleteFrame
{
	Width = BUFFER_WIDTH;
	Height = BUFFER_HEIGHT;
	Format = RGBA8;
};
sampler	samplerPreviousCompleteFrame
{
	Texture = texPreviousCompleteFrame;
};

texture	texPreviousColor0
{
	Width = BUFFER_WIDTH;
	Height = BUFFER_HEIGHT;
	Format = RGBA8;
};
sampler	samplerPreviousColor0
{
	Texture = texPreviousColor0;
};

texture	texPreviousColor1
{
	Width = BUFFER_WIDTH;
	Height = BUFFER_HEIGHT;
	Format = RGBA8;
};
sampler	samplerPreviousColor1
{
	Texture = texPreviousColor1;
};

texture	texPreviousColor2
{
	Width = BUFFER_WIDTH;
	Height = BUFFER_HEIGHT;
	Format = RGBA8;
};
sampler	samplerPreviousColor2
{
	Texture = texPreviousColor2;
};

texture	texPreviousColor3
{
	Width = BUFFER_WIDTH;
	Height = BUFFER_HEIGHT;
	Format = RGBA8;
};
sampler	samplerPreviousColor3
{
	Texture = texPreviousColor3;
};

uniform float framecount < source = "framecount"; >;


float3 GetPixel(float2 texcoord, int age)
{
	if ((framecount + iUIFrameOffset + age) % 4 <= 0.1)
		return tex2D(ReShade::BackBuffer, texcoord).rgb;
	else if ((framecount + iUIFrameOffset + age + 1) % 4 <= 0.1)
		return tex2D(samplerPreviousColor1, texcoord).rgb;
	else if ((framecount + iUIFrameOffset + age + 2) % 4 <= 0.1)
		return tex2D(samplerPreviousColor2, texcoord).rgb;
	else
		return tex2D(samplerPreviousColor3, texcoord).rgb;
}



void PS_RenderPreviousFrames(in float4 position : SV_Position, in float2 texcoord : TEXCOORD, out float3 color : SV_Target)
{
	color = tex2D(samplerCompleteFrame, texcoord).rgb;
}

void PS_UpdateStoreCompleteFrame(in float4 position : SV_Position, in float2 texcoord : TEXCOORD, out float4 completeFrame : SV_Target0)
{
	if (framecount % 4 <= 0.1)
	{
		if (texcoord.x <= 0.25)
		{
			completeFrame = GetPixel(float2(texcoord.x * 4, texcoord.y), 0);
		}
		else if (texcoord.x <= 0.5)
		{
			completeFrame = GetPixel(float2((texcoord.x - 0.25) * 4, texcoord.y), 1);
		}
		else if (texcoord.x <= 0.75)
		{
			completeFrame = GetPixel(float2((texcoord.x - 0.5) * 4, texcoord.y), 2);
		}
		else
		{
			completeFrame = GetPixel(float2((texcoord.x - 0.75) * 4, texcoord.y), 3);
		}
	}
	else
	{
		completeFrame = tex2D(samplerPreviousCompleteFrame, texcoord).rgb;
	}
}


void PS_StorePreviousFrames0(in float4 position : SV_Position, in float2 texcoord : TEXCOORD, out float4 previous0 : SV_Target0)
{
	previous0 = tex2D(ReShade::BackBuffer, float2(texcoord.x, texcoord.y));
}

void PS_StorePreviousFrames1(in float4 position : SV_Position, in float2 texcoord : TEXCOORD, out float4 previous0 : SV_Target0)
{
	previous0 = tex2D(samplerPreviousColor0, float2(texcoord.x, texcoord.y));
}

void PS_StorePreviousFrames2(in float4 position : SV_Position, in float2 texcoord : TEXCOORD, out float4 previous0 : SV_Target0)
{
	previous0 = tex2D(samplerPreviousColor1, float2(texcoord.x, texcoord.y));
}

void PS_StorePreviousFrames3(in float4 position : SV_Position, in float2 texcoord : TEXCOORD, out float4 previous0 : SV_Target0)
{
	previous0 = tex2D(samplerPreviousColor2, float2(texcoord.x, texcoord.y));
}

technique PreviousFrames <
	ui_tooltip = "Shader showing depth on half of a screen";
>
{
	pass prevPass3
	{
		VertexShader = PostProcessVS;
		PixelShader = PS_StorePreviousFrames3;

		RenderTarget0 = texPreviousColor3;
	}
	pass prevPass2
	{
		VertexShader = PostProcessVS;
		PixelShader = PS_StorePreviousFrames2;

		RenderTarget0 = texPreviousColor2;
	}
	pass prevPass1
	{
		VertexShader = PostProcessVS;
		PixelShader = PS_StorePreviousFrames1;

		RenderTarget0 = texPreviousColor1;
	}
	pass prevPass0
	{
		VertexShader = PostProcessVS;
		PixelShader = PS_StorePreviousFrames0;

		RenderTarget0 = texPreviousColor0;
	}
	pass renderPreviousCompleteFrame
	{
		VertexShader = PostProcessVS;
		PixelShader = PS_RenderPreviousFrames;

		RenderTarget0 = texPreviousCompleteFrame;
	}
	pass renderCompleteFrame
	{
		VertexShader = PostProcessVS;
		PixelShader = PS_UpdateStoreCompleteFrame;

		RenderTarget0 = texCompleteFrame;
	}
	pass
	{
		VertexShader = PostProcessVS;
		PixelShader = PS_RenderPreviousFrames;
	}
}
