import numpy as np
import math
import sys
import torch
import pygame
import random
import platform
import PIL
from PIL import Image
from pygame import locals as l
from torchvision import transforms
from OpenGL import GL
from OpenGL import GLU
import scipy
from scipy import ndimage

if platform.system() == "Windows":
    from screen_capture import capture_game_images
else:

    def capture_game_images(*kwargs):
        print("Capturing images is available only on Windows")
        return None


TARGET_SIZE = 224
far_plane_z = 50.0


def get_device():
    if torch.cuda.is_available():
        dev = "cuda"
        print("Using cuda")
    else:
        dev = "cpu"
        print("Using cpu")
    return torch.device(dev)


def load_image_tensor(path):
    pil_image = Image.open(path).resize((224,224)).convert('RGB')
    image = transforms.ToTensor()(pil_image)
    return image.to(device)


def predict_images(images):
    with torch.no_grad():
        predictions = model(torch.stack(images).to(device).view([len(images), 3, TARGET_SIZE, TARGET_SIZE]))
        predictions = np.clip(predictions.view([len(images), TARGET_SIZE, TARGET_SIZE]).cpu().numpy(), 0.1, 1.0)
        return predictions
        # f = predictions
        # blurred_f = ndimage.gaussian_filter(f, 3)
        # filter_blurred_f = ndimage.gaussian_filter(blurred_f, 1)
        # alpha = 30
        # sharpened = blurred_f + alpha * (blurred_f - filter_blurred_f)
        # return sharpened
    
def get_normalized_directions(rotation = 0):
    fov_y = 90.0/180.0 * math.pi
    fov_x = 121.0/180.0 * math.pi

    far_plane_y_up = math.sin(fov_y/2)
    far_plane_y_down = math.sin(-fov_y/2)

    far_plane_x_left = math.cos(rotation/180*math.pi + fov_x/2)
    far_plane_x_right = math.cos(rotation/180*math.pi - fov_x/2)
    far_plane_z_left = math.sin(rotation/180*math.pi + fov_x/2)
    far_plane_z_right = math.sin(rotation/180*math.pi - fov_x/2)

    points = np.zeros([TARGET_SIZE, TARGET_SIZE, 3])
    for x_index in range(0, TARGET_SIZE):
        for y_index in range(0, TARGET_SIZE):
            h_a = x_index / TARGET_SIZE
            y_a = y_index / TARGET_SIZE
            far_plane_pos = [
                far_plane_x_left * h_a + far_plane_x_right * (1.0 - h_a),
                far_plane_y_down * y_a + far_plane_y_up * (1.0 - y_a),
                far_plane_z_left * h_a + far_plane_z_right * (1.0 - h_a),
            ]
            # alternative curve method
            # far_plane_pos /= np.linalg.norm(far_plane_pos)
            points[y_index][x_index] = far_plane_pos

    return points


def get_transformed_points(normalized_direction, depth):
    points = np.zeros([TARGET_SIZE, TARGET_SIZE, 3])
    # points[:,:,0] = normalized_direction[:,:,0] * depth
    # points[:,:,1] = normalized_direction[:,:,1] * depth
    # points[:,:,2] = normalized_direction[:,:,2] * depth

    C = 0.1
    depth = depth * far_plane_z
    depth = (np.exp(depth * math.log(C + 1.0)) - 1.0) / C
    points[:, :, 0] = normalized_direction[:, :, 0] * depth
    points[:, :, 1] = normalized_direction[:, :, 1] * depth
    points[:, :, 2] = normalized_direction[:, :, 2] * depth

    return points


def update_points_colors(images, original_color=True):
    if images == None:
        return None, None
    colors = []
    points = []

    predicted_images = predict_images(images)
    for depth, normalized_direction in zip(predicted_images, normalized_directions):
        points.append(
            get_transformed_points(normalized_direction, depth)[
                :200, 26 : 224 - 36, :
            ].reshape([-1, 3])
        )

    if original_color:
        for image in images:
            colors.append(image.permute([1, 2, 0]).cpu().numpy()[:200, 26 : 224 - 36])
    else:
        for depth in predicted_images:
            color = np.zeros([TARGET_SIZE, TARGET_SIZE, 3])
            color[:, :, 0] = depth
            color[:, :, 1] = depth
            color[:, :, 2] = depth
            colors.append(color[:200, 26 : 224 - 36])

    return points, colors


rotations = [0, 90, 180, 270]
normalized_directions = [get_normalized_directions(rot) for rot in rotations]

mouse_sensitivity = 0.2
device = get_device()
model = torch.load("./models/efficient_b4", map_location=torch.device("cpu")).to(
    device
)


def draw_points(points, colors):
    GL.glVertexPointerd(points)
    GL.glColorPointerd(colors)
    GL.glDrawArrays(GL.GL_POINTS, 0, len(points))


def main(image_paths=None):
    yaw = 90
    pitch = 0.0
    offset_x = 0.0
    offset_y = 0.0
    offset_z = 0.0
    original_color = True
    points = None
    colors = None

    pygame.init()
    display = (1600, 900)
    game_display = pygame.display.set_mode(display, l.DOUBLEBUF | l.OPENGLBLIT)

    GL.glEnable(GL.GL_VERTEX_ARRAY)
    GL.glEnable(GL.GL_COLOR_ARRAY)
    GL.glEnable(GL.GL_DEPTH_TEST)
    GL.glPointSize(12)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_TAB:
                    original_color = not original_color
                    print(f"changed original_color to {original_color}")
                    points = None

        # if left click
        mouse_move = pygame.mouse.get_rel()
        if pygame.mouse.get_pressed()[0]:
            yaw = (yaw + mouse_move[0] * mouse_sensitivity) % 360.0
            pitch = (pitch + mouse_move[1] * mouse_sensitivity) % 360.0
            yaw += mouse_move[0] * mouse_sensitivity
        # if middle click
        elif pygame.mouse.get_pressed()[1]:
            offset_x += mouse_move[0] * mouse_sensitivity
            offset_y -= mouse_move[1] * mouse_sensitivity
        # if right click
        elif pygame.mouse.get_pressed()[2]:
            offset_z -= mouse_move[1] * mouse_sensitivity

        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)

        GL.glLoadIdentity()
        GLU.gluPerspective(60, (display[0] / display[1]), 0.1, 100000.0)
        GL.glTranslatef(offset_x, offset_y, offset_z)

        GL.glRotatef(pitch, 1.0, 0.0, 0.0)
        GL.glRotatef(yaw, 0.0, 1.0, 0.0)

        if image_paths:
            if not points:
                images = []
                for image_path in image_paths:
                    images.append(load_image_tensor(image_path).to(device))
                points, colors = update_points_colors(images, original_color)
        else:
            new_points, new_colors = update_points_colors(
                capture_game_images(), original_color
            )
            if new_points != None:
                points = new_points
                colors = new_colors
        if points == None:
            continue
        for point, color in zip(points, colors):
            draw_points(point, color)

        pygame.display.flip()
        pygame.time.wait(10)


if __name__ == "__main__":
    main(sys.argv[1:])
