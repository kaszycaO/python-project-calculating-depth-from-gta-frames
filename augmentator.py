import random
from math import ceil

from PIL import Image
from torchvision import transforms
import torchvision.transforms.functional as TF


def do_crop(images, param):
    width, height = images[0].size
    new_height = height - (height * param / 100)
    new_width = width - (width * param / 100)
    transform = transforms.RandomCrop(
        (new_height, new_width), pad_if_needed=True, padding_mode="edge"
    )
    return [transform(image) for image in images]


def do_rotation(images, param):
    angle = random.uniform(-param, param)
    return [TF.rotate(image, angle, expand=True) for image in images]


def do_horizontal(images, param):
    if random.random() > param:
        return [TF.hflip(image) for image in images]
    else:
        return images


def augmentation(images, crop=0, rotate=0, translate=0, shear=0, horizontal=0):
    """ Data augmentation for PIL Images

    Arguments:

        img: PIL Image

        crop: integer in (0, 100)
              crop random an image in %

        rotate: integer
                rotate random an image in degrees

        translate: integer in (0, 100)
                   translate random an image in %

        shear: integer
               shear random an image, higher value -> more changes

        horizontal: float in (0, 1)
              flip horizontal an image with probability

    Return:

        modified copy of image

    """

    images = do_crop(images, crop)
    images = do_rotation(images, rotate)
    images = do_horizontal(images, horizontal)

    return images
